# ORID

## O

Flyway: In Flyway, learned how to manage the history of database changes, how to implement version control, rollbacks, and migrations. Also, learned how to use Flyway to coordinate the process of database upgrades and iterations within a development team.
Mapper: In Mapper, learn about the mapping relationship between DTO and Entity and how to use the Mapper tool for mapping conversion.
Cloud Native: Learn what microservices, containers, CICD, and other concepts are and their impact and value to modern software development in a cloud-native related study.
Trainee Session: In today's study, we learned about the basic concepts and application scenarios of container technology. Container technology is a lightweight virtualization technology that enables virtualization at the operating system level, allowing an application and the runtime environment it relies on (such as the operating system, library files, configuration files, etc.) to be packaged into a portable container, thus achieving cross-platform, rapid deployment and reliable operation.
Docker: Docker is one of the most popular container technologies today. With Docker, we can quickly create, deploy and manage containerized applications for continuous integration, continuous delivery and rapid iteration.
Team Review: Through team review and group discussion, recognize what we did well and what we didn't do well, and improve on it in the future.

## R

full

## I

I don't have a deep grasp of the fundamentals of containers myself, and I didn't demonstrate them well in the trainee session phase. Still need to strengthen the understanding.lack of knowledge of CICD, need to learn independently.

## D

​	I hope to be able to utilize what I've learned today in future development, such as knowledge of microservices, containers, cicd, and so on.
