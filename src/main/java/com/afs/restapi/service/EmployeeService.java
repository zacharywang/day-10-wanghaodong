package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;
    private  final EmployeeJPARepository employeeJPARepository;
    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public EmployeeJPARepository getEmployeeJPARepository(){
        return employeeJPARepository;
    }

    public List<Employee> findAll() {

        return getEmployeeJPARepository().findAll();
    }

    public EmployeeResponse findById(Long id) {
        Employee employee = getEmployeeJPARepository().findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employee);
    }

    public EmployeeResponse update(Long id, Employee employee) {
        EmployeeResponse toBeUpdatedEmployee = findById(id);
        Employee employeeTemp = new Employee();
        BeanUtils.copyProperties(toBeUpdatedEmployee,employeeTemp);
        if (employee.getSalary() != null) {
            employeeTemp.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            employeeTemp.setAge(employee.getAge());
        }
        return EmployeeMapper.toResponse(getEmployeeJPARepository().save(employeeTemp));
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeJPARepository.findByGender(gender);

    }

    public EmployeeResponse create(Employee employee) {
        Employee save = employeeJPARepository.save(employee);
        return EmployeeMapper.toResponse(save);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeJPARepository.findAll(Pageable.ofSize(size).withPage(page-1)).stream().collect(Collectors.toList());
        //return getEmployeeRepository().findByPage(page, size);
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}
